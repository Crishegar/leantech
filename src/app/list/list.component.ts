import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  items: Iitem[] = [
    {
      date: 'Monday 10',
      time: '2:28 PM',
      from: 'Huston, TX, 33619',
      to: 'Atlanta, GA, 30123',
      price: 250,
      notifies: 1
    },
    {
      date: 'Monday 10',
      time: '2:28 PM',
      from: 'Huston, TX, 33619',
      to: 'Atlanta, GA, 30123',
      price: 250,
      notifies: 1
    },
    {
      date: 'Monday 10',
      time: '2:28 PM',
      from: 'Huston, TX, 33619',
      to: 'Atlanta, GA, 30123',
      price: 250,
      notifies: 1
    },
    {
      date: 'Monday 10',
      time: '2:28 PM',
      from: 'Huston, TX, 33619',
      to: 'Atlanta, GA, 30123',
      price: 250,
      notifies: 1
    }
  ];
  constructor() { }

  ngOnInit() {
  }

}

export interface Iitem {
  date;
  time;
  from;
  to;
  price;
  notifies;
}
