import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'money'
})
export class MoneyPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    const format = `$ ${parseFloat(value).toFixed(2)}`;
    return format;
  }

}
