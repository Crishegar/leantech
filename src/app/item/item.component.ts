import { Component, OnInit, Input } from '@angular/core';
import { Iitem } from '../list/list.component';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() item: Iitem;

  constructor() { }

  ngOnInit() {
  }

}
