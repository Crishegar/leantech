import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { SideComponent } from './side/side.component';
import { FilterComponent } from './filter/filter.component';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './item/item.component';
import { MoneyPipe } from './money.pipe';
import { IconButtonComponent } from './icon-button/icon-button.component';
import { ToggleComponent } from './toggle/toggle.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SideComponent,
    FilterComponent,
    ListComponent,
    ItemComponent,
    MoneyPipe,
    IconButtonComponent,
    ToggleComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
