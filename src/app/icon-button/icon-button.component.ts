import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'lt-icon-button',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss']
})
export class IconButtonComponent implements OnInit {

  @Input() icon = 'check';
  @Input() size;
  class: string;

  constructor() {
   }

  ngOnInit() {
    this.class = `fa-${this.icon}`;
  }

}
